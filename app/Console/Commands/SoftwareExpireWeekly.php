<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SoftwareLicense;
use App\Models\Project;
use App\Models\Client;
use App\Models\User;


class SoftwareExpireWeekly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'software-licence:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email whom Licence is expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Send 15 day mail
        $data=$this->_GetsoftwareData(15);
        if($data==1){
            echo "</br>cron running out successfully";
        }else{
            echo "<br/>no records found !";
        }

        //Send 7 days daily mail
        $data=$this->_GetsoftwareData(7);
        if($data==1){
            echo "</br>cron running out successfully";
        }else{
            echo "<br/>no records found !";
        }
        exit;
    }
    public static function _GetsoftwareData($expireDays){
        $clientEmails="";
        $today =  date("j M Y");
        if($expireDays==15){
            $expireDate=date('Y-m-d', strtotime($expireDays.' days'));

            $software_licence = SoftwareLicense::where('expiry_date',$expireDate)->get();
            if(!empty($software_licence))
            {
                $i=1;
                $table="";
                foreach ($software_licence as $key => $PrjValue) 
                {
                    $toEmails = [];
                   
                    $table = "<p><b>Hi All,</b></p>";
                    $table .= "<p> Your licence is expiring  in ".$expireDays." Days -".$expireDate."</p>";
                    $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="font-size:13px; border-top:1px solid #666; border-left:1px solid #666; font-family:Arial, Helvetica, sans-serif;">';
                    $table .= "<tr>";
                    $table .= '<td width="10%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Sr. No.</td>';
                    $table .= '<td width="30%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Project Name</td>';
                    $table .= '<td width="30%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Expiry Date</td>';
                    $table .= '<td width="30%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Licenses Type</td><tr/>';        
            
                    //Projetc data
                    $projectsName = $clientId = '';
                    $prjData = Project::where('id',$PrjValue->project_id)->first();
                    if($prjData){
                        $projectsName = $prjData->title;
                        $clientId = $prjData->client_id;
                    }
                    $expiry_date = $PrjValue->expiry_date;
                    $licence_type = $PrjValue->licence_type;
                      
                    $userEmails = User::where('is_expire_licence_mail',1)->where('id',"!=", SUPER_ADMIN_ID)->where('status',1)->pluck('email')->all();
                    $clientEmails = Client::where('id',$clientId)->pluck('email')->all();
                    
                    $toEmails = array_merge($userEmails,$clientEmails);
                    $comaJoin=$bccEmail="";
                    $table .= "<tr>";
                    $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$i."</td>";
                    $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$projectsName."</td>";
                    $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$expiry_date."</td>";
                    $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$licence_type."</td><tr/>";
                    $table .= "</table>";
                    $i++;
                    $subject = 'Reports PHPdots: Your licence is expiring on '.$expiry_date;
                    $params["to"] = 'snehalpatil.phpdots@gmail.com';
                    $params["ccEmails"] = $toEmails;
                    $params["subject"] = $subject;
                    $params["body"] = "<html><body>".$table."</body></html>";
                    sendHtmlMail($params);                                                  
                }
                return 1;
            }else{
                return 0;
            }
        }else{
            $software_licence=SoftwareLicense::select("*",\DB::raw("DATEDIFF(expiry_date, NOW()) as date_diff"))->where('status',1)->orderBy(\DB::raw("(case when date_diff < 0 then 1 else 0 end),abs(date_diff)"), 'asc')->get();
            if(!empty($software_licence))
            {
                $i=1;
                $table="";
                foreach ($software_licence as $key => $PrjValue)
                {
                    $toEmails = [];
                    if($PrjValue->date_diff<=7 && $PrjValue->date_diff>=0){
                        $table = "<p><b>Hi All,</b></p>";
                        $table .= "<p> Your licence is expiring in ".$PrjValue->date_diff." Days-".$PrjValue->expiry_date."</p>";
                        $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="font-size:13px; border-top:1px solid #666; border-left:1px solid #666; font-family:Arial, Helvetica, sans-serif;">';
                        $table .= "<tr>";
                        $table .= '<td width="10%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Sr. No.</td>';
                        $table .= '<td width="30%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Project Name</td>';
                        $table .= '<td width="30%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Expiry Date</td>';
                        $table .= '<td width="30%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Licence Type</td><tr/>';
            
                        //Projetc data
                        $projectsName = $clientId = '';
                         $prjData = Project::where('id',$PrjValue->project_id)->first();
                        if($prjData){
                            $projectsName = $prjData->title;
                            $clientId = $prjData->client_id;
                        }
                        $expiry_date = $PrjValue->expiry_date;
                        $licence_type = $PrjValue->licence_type;
                        
                        $userEmails = User::where('is_expire_licence_mail',1)->where('id',"!=", SUPER_ADMIN_ID)->where('status',1)->pluck('email')->all();
                        $clientEmails = Client::where('id',$clientId)->pluck('email')->all();
                        
                        $toEmails = array_merge($userEmails,$clientEmails);
                        $comaJoin=$bccEmail="";
                        $table .= "<tr>";
                        $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$i."</td>";
                        $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$projectsName."</td>";
                        $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$expiry_date."</td>";
                        $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$licence_type."</td><tr/>";
                        $table .= "</table>";
                        $subject = 'Reports PHPdots: Your licence is expiring on '.$expiry_date;
                        $params["to"] = 'snehalpatil.phpdots@gmail.com';
                        $params["ccEmails"] = $toEmails;
                        $params["subject"] = $subject;
                        $params["body"] = "<html><body>".$table."</body></html>";
                        sendHtmlMail($params);
                    }
                }
                 $i++;
                return 1;
            }else{
                return 0;
            } 
        }
    }
}
