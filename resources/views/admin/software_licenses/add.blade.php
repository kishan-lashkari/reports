@extends('admin.layouts.app')
@section('content')

<div class="page-content">
    <div class="container">
        <div class="row autoResizeHeight">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-text-o"></i>
                           {{ $page_title }}
                        </div>
                        <a class="btn btn-default pull-right btn-sm mTop5" href="{{ $back_url }}">Back</a>
                    </div>
                    <div class="portlet-body">
                        <div class="form-body">
                            {!! Form::model($formObj,['method' => $method,'files' => true, 'route' => [$action_url,$action_params],'class' => 'sky-form form form-group', 'id' => 'main-frm1']) !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div data-repeater-list="group-a">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="mt-repeater-input">
                                                <label class="control-label">Project</label>
                                                {!! Form::select('project_id',[''=>'Select Project']+$projects,null,['class' => 'select2-common project_id form-control', 'data-required' => true,'id'=>'project_id']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Licence Type:<span class="required">*</span></label>
                                    {!! Form::select('licence_type',[''=>'Select Licence Type','SSL'=>'SSL','Software'=>'Software','Other'=>'Other'],Request::get("search_licence"),['class' => 'form-control', 'data-required' => true]) !!}
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label">Licence Web URL: <span class="required">*</span></label>
                                    {!! Form::text('url',null,['class' => 'form-control', 'data-required' => true,'placeholder' => 'Enter URL']) !!}
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">User Name:<span class="required">*</span></label>
                                    {!! Form::text('user_name',null,['class' => 'form-control', 'data-required' => True,'placeholder' => 'Enter user_name']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Password:<span class="required">*</span></label>
                                    {!! Form::text('password',null,['class' => 'form-control', 'data-required' => True,'placeholder' => 'Enter Password']) !!}
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Others:</label>
                                

                                    {!! Form::textarea('others',null,['class' => 'form-control','rows'=>1,'placeholder'=>'Enter Other']) !!}
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Expiry Date:<span class="required">*</span></label>
                                    <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                        {!! Form::text('expiry_date',null,['class' => 'form-control', 'data-required' => true,'placeholder' => 'Select Expiry Date','id'=>'end_date']) !!}
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Status:</label>
                                    <div>
                                        {!! Form::radio('status','1',true)!!}
                                        <label class="control-label">Active </label>
                                        
                                        {!! Form::radio('status','0')!!}
                                        <label class="control-label"> Inactive </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Save" class="btn btn-success pull-right" id="submit_btn" />
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
    </div>
</div>            


@endsection

@section('scripts')
<script type="text/javascript">
function reinitselect2()
{
$("select.select2-common").select2({
placeholder: "Search",
allowClear: true,
minimumInputLength: 2,
width: null
});
}
$(document).ready(function () {
reinitselect2();
$('#main-frm1').submit(function () {
if ($(this).parsley('isValid'))
{
$('#submit_btn').attr("disabled", true);
$('#AjaxLoaderDiv').fadeIn('slow');
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: new FormData(this),
contentType: false,
processData: false,
enctype: 'multipart/form-data',
success: function (result)
{
$('#AjaxLoaderDiv').fadeOut('slow');
if (result.status == 1)
{
$.bootstrapGrowl(result.msg, {type: 'success', delay: 4000});
window.location = result.goto;
}
else
{
$.bootstrapGrowl(result.msg, {type: 'danger', delay: 4000});
$('#submit_btn').attr('disabled', false);
}
},
error: function (error) {
$('#AjaxLoaderDiv').fadeOut('slow');
$.bootstrapGrowl("Internal server error !", {type: 'danger', delay: 4000});
}
});
}
return false;
});
});
</script>
{{-- <script src="{{ asset("themes/admin/assets/")}}/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>
<script src="{{ asset("themes/admin/assets/")}}/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
<script src="{{ asset("themes/admin/assets/")}}/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script> --}}
@endsection
