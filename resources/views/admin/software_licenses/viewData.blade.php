 
@foreach($views as $view)
<div class="portlet blue-hoki box">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i>Software Licence</div>
		</div>
		<div class="portlet-body">
			<div class="row static-info">
				<div class="col-md-5 name"> Project Name: </div>
				<div class="col-md-7 value"> {{$view->project_name}} </div>
			</div>
			<div class="row static-info">
				<div class="col-md-5 name"> Licence Type: </div>
				<div class="col-md-7 value"> {{$view->licence_type}}</div>
			</div>

			<div class="row static-info">
				<div class="col-md-5 name"> Licence Web URL: </div>
				<div class="col-md-7 value">
					@if(!empty($view->url))
					<input type="text" id="weburl_copy_url" value="{{$view->url}}" />
					<button onclick="copy('url');"  style="margin-right: 20px;" title="Click To Copy" class="copy-button btn btn-xs"><i class="fa fa-clipboard" aria-hidden="true"></i></button>
					@endif
				</div>
			</div>
			
			<div class="row static-info">
				<div class="col-md-5 name">User Name:</div>
				<div class="col-md-7 value">
					@if(!empty($view->user_name))
					<input type="text" id="weburl_copy_user_name" value="{{$view->user_name}}" />
					<button  onclick="copy('user_name');" data-clipboard-target="#user_name_copy_{{ $view->id}}"  style="margin-right: 20px;" title="Click To Copy" class="copy-button btn btn-xs"><i class="fa fa-clipboard" aria-hidden="true"></i></button>
					@endif
				</div>
			</div>
			<div class="row static-info">
				<div class="col-md-5 name"> Password:</div>
				<div class="col-md-7 value"> 
					@if(!empty($view->password))
					<input type="text" id="weburl_copy_password" value="{{$view->password}}" />
					<button onclick="copy('password');" data-clipboard-target="#password_copy_{{ $view->id}}"  style="margin-right: 20px;" title="Click To Copy" class="copy-button btn btn-xs"><i class="fa fa-clipboard" aria-hidden="true"></i></button>
					@endif
				</div>
			</div>
			<div class="row static-info">
				<div class="col-md-5 name"> Other: </div>
				<div class="col-md-7 value"> {{$view->others}} </div>
			</div>
			<div class="row static-info">
				<div class="col-md-5 name"> Expiry Date: </div>
				<div class="col-md-7 value"> {{$view->expiry_date}} </div>
			</div>
			<div class="row static-info">
				<div class="col-md-5 name"> Status: </div>
				<div class="col-md-7 value"> @if($view->status == 1) <div class="btn btn-xs btn-success"> Active</div> @else <div class="btn btn-xs btn-danger">In Active</div> @endif</div>
			</div>
		</div>
	</div>
	@endforeach
	<script type="text/javascript">

		function copy(id){
			var message = $("#weburl_copy_"+id).val();
			/* Get the text field */
			var copyText = document.getElementById("weburl_copy_"+id);

			/* Select the text field */
			copyText.select();

			/* Copy the text inside the text field */
			document.execCommand("copy");

			/* Alert the copied text */
			/*alert("Copied the text: " + copyText.value);*/
		}
	</script>