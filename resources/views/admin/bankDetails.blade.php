 @extends('admin.layouts.app')

@section('content')
 
<div class="page-content">
<div class="container">

<div class="portlet blue-hoki box">
	<div class="portlet-title">
		<div class="caption">
		<i class="fa fa-university"></i>Company Bank Detail </div>
	</div>
	<div class="portlet-body">
		<div class="row static-info">
			<div class="col-md-5 name"> Account Name: </div>
			<div class="col-md-7 value"> {{ BANK_ACC_NAME }} </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Bank: </div>
			<div class="col-md-7 value"> {{ BANK_NAME }}</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Account No: </div>
			<div class="col-md-7 value"> {{ BANK_ACC_NO }} </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Address: </div>
			<div class="col-md-7 value"> {{ BANK_ADDRESS }} </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> CITY: </div>
			<div class="col-md-7 value"> AHMEDABAD</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> State: </div>
			<div class="col-md-7 value">GUJARAT</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> District: </div>
			<div class="col-md-7 value"> AHMADABAD</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Branch: </div>
			<div class="col-md-7 value"> AHMEDABAD - 100 FEET ROAD</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> IFSC Code: </div>
			<div class="col-md-7 value">  {{ BANK_IFSC_CODE }} (used for RTGS, IMPS and NEFT transactions)</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Branch Code: </div>
			<div class="col-md-7 value">  {{ BANK_BRANCH_CODE }} (Last six characters of IFSC Code represent Branch code.)</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> SWIFT Code: </div>
			<div class="col-md-7 value">  {{ BANK_SFIWT_CODE }} (Last six characters of IFSC Code represent Branch code.)</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Branch Code: </div>
			<div class="col-md-7 value"> {{ BANK_BRANCH_CODE }}</div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> MICR Code: </div>
			<div class="col-md-7 value"> {{ BANK_MICR_CODE }} </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Email: </div>
			<div class="col-md-7 value"> jitendra.rathod@phpdots.com </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Phone no: </div>
			<div class="col-md-7 value"> +91 9825096687 </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> Pan no: </div>
			<div class="col-md-7 value">  {{ PAN_NO }} </div>
		</div>
		<div class="row static-info">
			<div class="col-md-5 name"> GST no: </div>
			<div class="col-md-7 value">  {{ GST_REGI_NO }} </div>
		</div>
	</div>
</div>

</div>
</div>

@endsection
 