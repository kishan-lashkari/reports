@extends('admin.layouts.app')

@section('content')

<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <div class=""> 
            @include("admin.project_hours.search")
            <div class="clearfix"></div>
			<a class="btn btn-default pull-right">
              Total Hours: <span id="overall-hours"> 0 </span>
            </a>    
            <div class="clearfix">&nbsp;</div>
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>{{ $page_title }}    
                    </div> 
                </div>
                <div class="portlet-body">                    
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="server-side-datatables">
                        <thead>
                            <tr>
                               <th width="2%">ID</th>
                               <th width="20%">Client Name</th>
                               <th width="30%">Project</th> 
                               <th width="10%">Hours</th> 
                               <th width="15%">Month</th> 
                            </tr>
                        </thead>                                         
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>              
        </div>
    </div>
</div> 
@endsection
@section('styles')
  
@endsection

@section('scripts')
    <script type="text/javascript">
    $(document).ready(function(){ 
        $(".search_select").select2({
            placeholder: "Search Here",
            allowClear: true,
            minimumInputLength: 2,
            width: null
        });
         $("#search-frm").submit(function(){
            oTableCustom.draw();
            return false;
        });

        $.fn.dataTableExt.sErrMode = 'throw';

        var oTableCustom = $('#server-side-datatables').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            lengthMenu:
              [
                [25,50,100,150,200],
                [25,50,100,150,200]
              ],
            ajax: {
                "url": "{!! route('projects-hour.data') !!}",
                "data": function ( data ) 
                {
                    data.search_client = $("#search-frm select[name='search_client']").val();
                    data.search_project = $("#search-frm select[name='search_project']").val();
                    data.search_task_date = $("#search-frm select[name='search_task_date']").val();
                },
                dataSrc: function(response){
                    $("#overall-hours").html(response.hours);
                    $("#is_total").val(response.hours);
                    return response.data;
                }
            },            
            "order": [[ '0', "desc"  ]],   
            columns: [
                { data: 'id', name: 'id' },
                { data: 'client_name', name: 'client_name' },
                { data: 'project_name', name: 'project_name' },
                { data: 'total_hour', name: 'total_hour' },
                { data: 'month', name: 'month'},
            ]
        });
    });
    </script>
@endsection

