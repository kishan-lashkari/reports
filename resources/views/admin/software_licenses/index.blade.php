@extends('admin.layouts.app')
<?php
$popup_id = request()->get('popup_id');
?>
@section('content')
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <div class="">
            
            @include($moduleViewName.".search")
            <div class="clearfix"></div>
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>{{ $page_title }}
                    </div>
                    
                    @if($btnAdd)
                    <a class="btn btn-default pull-right btn-sm mTop5" href="{{ $add_url }}">Add New</a>
                    @endif
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-striped table-condensed flip-content" id="server-side-datatables">
                        <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="20%">Project Name</th>
                                <th width="20%">Licence Type</th>
                                <th width="10%">Expiry Date</th>
                                <th width="5%" data-orderable="false">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade bs-modal-lg" id="software_view" role="dialog" tabindex="-1" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Software Licence Detail</h4>
        </div>
        <div class="modal-body">
            <table class="table" id="software_detail_table">
                
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    
</div>
</div>
@endsection
@section('styles')

@endsection
@section('scripts')
<script type="text/javascript">
$(window).load(function(){
setTimeout(function(){
@if(!empty($popup_id))
openView({{$popup_id}});
@endif
},3500);
});
function openView($id){
jQuery('#software_view').modal();
$('#AjaxLoaderDiv').fadeIn('slow');

var software_url="{{ url('/software-licenses/view') }}";
$.ajax({
type: "GET",
url: software_url,
data:
{
software_id: $id
},
success: function (result)
{
$("#software_detail_table").html(result);
$('#AjaxLoaderDiv').fadeOut('slow');
setTimeout(function(){
(function(){
new Clipboard('.copy-button');
})();
},1000)

},
error: function (error)
{
$('#AjaxLoaderDiv').fadeOut('slow');
}
});
}
$(document).ready(function(){
$("#project_id").select2({
placeholder: "Search Project",
allowClear: true,
minimumInputLength: 2,
width: null
});
$("#search-frm").submit(function(){
oTableCustom.draw();
return false;
});
$.fn.dataTableExt.sErrMode = 'throw';
var oTableCustom = $('#server-side-datatables').DataTable({
processing: true,
serverSide: true,
searching: false,
//info: false,

ajax: {
"url": "{!! route($moduleRouteText.'.data') !!}",
"data": function (data)
{
data.search_project = $("#search-frm select[name='search_project']").val();
/*data.search_project = $("#search-frm input[name='search_project']").val();*/
data.search_licence = $("#search-frm select[name='search_licence']").val();
}
},
lengthMenu:
[
[100,200,300,400,500],
[100,200,300,400,500]
],
"order": [['0',"desc"  ]],
columns: [
{ data: 'id', name: 'id' },
{ data: 'project_name', name: '{{ 'projects' }}.modal-title' },
{ data: 'licence_type', name: 'licence_type' },
{ data: 'expiry_date', name: 'expiry_date' },
{ data: 'action', orderable: false, searchable: false}
]
});
});
</script>
@endsection