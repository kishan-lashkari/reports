<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoftwareLicense extends Model
{
    public $timestamps = true;
    protected $table = TBL_SOFTWARE_LICENSE;

    /**
     * @var array
     */
    protected $fillable = ['id','project_id','licence_type','url','user_name','password','others','expiry_date','status'];

    public function project(){
        return $this->belongsTo('App\Models\Project','project_id','id');
    }
   /* public function Project()
    {
        return $this->hasMany('App\Models\Project');
    }*/
    public function GetProject() {
        $result = \DB::table(TBL_PROJECT)
                ->select(\DB::raw("title,id"))
                ->pluck('title', 'id')
                ->toArray();
        $resultArray = array();
        if ($result) {
            $resultArray = json_decode(json_encode($result), true);
        }
        return $resultArray;
    }

    public static function softwareExpirydate(){
        $data = SoftwareLicense::select(TBL_SOFTWARE_LICENSE.'.*',\DB::raw('datediff(expiry_date, CURDATE()) as DIFF, projects.title as project_name'))
                ->join('projects','projects'.'.id',"=",TBL_SOFTWARE_LICENSE.'.project_id')
                ->get();
    } 

}
