@extends('admin.layouts.app')
<?php
$auth = Auth::guard('admins')->user()->user_type_id;
?>
@section('styles')
<!-- multiselect -->
<link href="{{ asset('/themes/admin/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

<div class="page-content">
    <div class="container">
        <div class="row autoResizeHeight">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-text-o"></i>
                            {{ $page_title }}
                        </div>
                        <a class="btn btn-default pull-right btn-sm mTop5" href="{{ $back_url }}">Back</a>
                    </div>
                    <div class="portlet-body">
                        <div class="form-body">
                           {!! Form::model($formObj,['method' => $method,'files' => true, 'route' => [$action_url,$action_params],'class' => 'sky-form form form-group', 'id' => 'main-frm1']) !!} 

                           <div class="row">                                
                            <div class="col-md-12">
                                <label class="control-label">Title: <span class="required">*</span></label>
                                {!! Form::text('title',null,['class' => 'form-control', 'data-required' => true,'placeholder' => 'Enter Project Title']) !!}
                            </div>
                        </div>
                        @if($auth == ADMIN_USER_TYPE)
                        <div class="clearfix">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Client Name: <span class="required">*</span></label>                            
                                {!! Form::select('client_id',[''=>'Select Client']+$clients,null,['class' => 'form-control', 'data-required' => true,'id'=>'client_id']) !!}
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Status: <span class="required">*</span></label>                            
                                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'],null,['class' => 'form-control', 'data-required' => true]) !!}
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">&nbsp;</label>  
                                <div class="md-checkbox">
                                    {{ Form::checkbox('send_email', 1, null, ['class' => 'md-check','id'=>'checkbox1']) }}
                                    <label for="checkbox1">
                                        <span></span>
                                        <span class="check" style="z-index: 1;"></span>
                                        <span class="box" ></span>
                                        Send Email Report To
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="portlet">
                                    <div class="portlet-body" style="display: none;" id="sendTypeDiv">
                                        <label class="control-label">&nbsp;</label>
                                        <br/>
                                    @if($isEdit == 0)
                                        <select class="mt-multiselect btn btn-default form-control" multiple="multiple" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true" name="sendMailClientUsers[]" id="multi_select_userclient" >
                                        </select>
                                    @endif

                                    @if($isEdit)
                                        <select class="mt-multiselect btn btn-default form-control" multiple="multiple" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true" name="sendMailClientUsers[]" id="multi_select_userclient" >
                                            @foreach($sendMailClientUsers as $cid => $c_name)
                                                <option value="{{$cid}}" {{ in_array($cid, $list_sendMailUsers) ? 'selected':''}}>{{ $c_name }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!-- For Ajax  -->
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <input type="hidden" name="ajaxurl" value="{{ route('projects.clientUser.data')}}" id="ajaxurl">
                            @endif
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Save" class="btn btn-success pull-right" />
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<!-- multiselect jquery -->
<script src="{{ asset('themes/admin/assets/pages/scripts/components-bootstrap-multiselect.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('themes/admin/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () { 
      $("#client_id").select2({
        placeholder: "Search Client Name",
        allowClear: true,
        minimumInputLength: 2,
        width: null
    });

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
      $('#client_id').change(function(){
        var client_id = $(this).val();
        $.ajax({
            type: "POST",
            url: "{{route('projects.clientUser.data')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "client_id" : client_id,
            },
            success: function(data) {
                console.log(data);   
                $("#multi_select_userclient").multiselect('destroy');
                $('#multi_select_userclient').find('option').remove();
                $('#multi_select_userclient').append(data);
                $('#multi_select_userclient').multiselect();
            },
            error:function(){
            }
        });             
    });

      var check_send_email = '{{ $formObj->send_email }}';
      if(check_send_email == 1)
      {
        $("#sendTypeDiv").show();
    }

    $("#checkbox1").click(function() {
        if($(this).is(":checked")) {
            $("#sendTypeDiv").show();
        } else {
            $("#sendTypeDiv").hide();
        }
    });

    $('#main-frm1').submit(function () {

        if ($(this).parsley('isValid'))
        {
            $('#AjaxLoaderDiv').fadeIn('slow');
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: new FormData(this),
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                success: function (result)
                {
                    $('#AjaxLoaderDiv').fadeOut('slow');
                    if (result.status == 1)
                    {
                        $.bootstrapGrowl(result.msg, {type: 'success', delay: 4000});
                        window.location = result.goto;    
                    }
                    else
                    {
                        $.bootstrapGrowl(result.msg, {type: 'danger', delay: 4000});
                    }
                },
                error: function (error) {
                    $('#AjaxLoaderDiv').fadeOut('slow');
                    $.bootstrapGrowl("Internal server error !", {type: 'danger', delay: 4000});
                }
            });
        }            
        return false;
    });
});
</script>
@endsection

