<div class="portlet blue-hoki box">
	<div class="portlet-title">
		<div class="caption">
		<i class="fa fa-file"></i>{{$name}} </div>
	</div>
	<div class="portlet-body">
			<div class="row static-info">
				<div class="col-md-12"> <b>1. Describe your duties and responsibilities.</b> </div>
				<div class="col-md-12"> <?php echo nl2br($appraisal->duty_responsibility);?> </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b>2. Total experience as on 31st December 2019. (Ex. 5 years and 6 months)</b> (Ex. 5 years and 6 months) </div>
				<div class="col-md-12"> {{$appraisal->years}} - Years  {{$appraisal->months}} - Months</div>
			</div>
			<hr />	
			<div class="row static-info">
				<div class="col-md-12"> <b>3. Rate your Past year (0-3 Bad, 4-7 Average, 8-10 Good)	</b> </div>
				<div class="col-md-12"> {{$appraisal->past_year_rate}}</div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b>4. List your most significant achievement since last year. How does this achievement align with the goal outlined in your last review?</b>  </div>
				<div class="col-md-12"> <?php echo nl2br($appraisal->achievements);?> </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b>5. Have you successfully performed any tasks or additional duties outside your regular responsibility, if so please specify.</b> </div>
				<div class="col-md-12"> <?php echo nl2br($appraisal->additional_duties);?> </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12" > <b>6.Rate yourself on all factor which describe below:</b> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Technical Skill </div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->technical_skills]))?$skills[$appraisal->technical_skills]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  English communication with client</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->english_communication]))?$skills[$appraisal->english_communication]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Requirement understanding without help</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->requirement_understanding]))?$skills[$appraisal->requirement_understanding]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Utilization of own productivity</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->utilization_productivity]))?$skills[$appraisal->utilization_productivity]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Team work</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->team_work]))?$skills[$appraisal->team_work]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Professionalism (Punctuality, attendance, responsiveness etc.)</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->professionalism]))?$skills[$appraisal->professionalism]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Can able to generate work from current client/ helping on client retention</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->generate_work]))?$skills[$appraisal->generate_work]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Work quality</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->work_quality]))?$skills[$appraisal->work_quality]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Git knowledge</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->git_knowledge]))?$skills[$appraisal->git_knowledge]:0; ?> </div>
			</div>
			<div class="row static-info" style="padding-left: 30px;">
				<div class="col-md-12" >  Attitude towards job/work</div>
				<div class="col-md-12"> <?php echo (isset($skills[$appraisal->attitude]))?$skills[$appraisal->attitude]:0; ?> </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b>7. How well do you prioritize and manage your workload?</b></div>
				<div class="col-md-12"> <?php echo nl2br($appraisal->manage_workload);?></div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b>8. Do you feel that your achievements were recognised and rewarded?</b> </div>
				<div class="col-md-12"> {{$appraisal->achievements_rewarded}} </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b> 9. Rate your job satisfaction (0-3 Bad, 4-7 Average, 8-10 Good)</b> </div>
				<div class="col-md-12"> {{$appraisal->job_satisfaction}} </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b>10. State your career goals for the coming year and describe how you plan to accomplish them.</b></div>
				<div class="col-md-12"> {{$appraisal->goal}} </div>
			</div>
			<hr />
			<div class="row static-info">
				<div class="col-md-12"> <b> 11. What things can the company do to better your working environment? </b></div>
				<div class="col-md-12"> {{$appraisal->suggestion}} </div>
			</div>
			<hr />
		</div>
</div>
