@extends('admin.layouts.app')


@section('content')

<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">

        <div class="">
            
            @include($moduleViewName.".search") 

            <div class="clearfix"></div>    
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>{{ $page_title }}    
                    </div>
                    @if($btnAdd)
                    <a class="btn btn-default pull-right btn-sm mTop5" href="{{ $add_url }}">Add New</a>
                    @endif        
                </div>
                <div class="portlet-body">                    
                    <table class="table table-bordered table-striped table-condensed flip-content" id="server-side-datatables">
                        <thead>
                            <tr>
                               <th width="5%">ID</th>
                               <th width="5%">Image</th>
                               <th width="20%">Firstname</th>
                               <th width="15%">Lastname</th>
                               <th width="15%">Email<br/> # User Type</th>
                               <th width="5%">Status<br/> # Paid Leave</th>
                               <th width="15%">Created At</th>
                               <th width="20%" data-orderable="false">Action</th>
                           </tr>
                       </thead>
                       <tbody>
                       </tbody>
                   </table>
               </div>
           </div>
       </div>
       <!-- Change Password popup Model -->
       <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

           <!--  Modal content -->
           <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="model_title"></h4>
            </div>
            <div class="modal-body ">
                <h4 class="text-danger text-center"></h4>
                <!--  Form Start-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-key"></i>
                            Change Password
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form" id="change_user_pass_frm" method="post">
                            {!! csrf_field() !!}    
                            <div class="form-body">
                                <input type="hidden" name="change_pass_id" value="" id="change_pass_id">
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" class="form-control" name="new_password" data-required="true"/> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" class="form-control" name="new_password_confirmation" data-required="true" /> 
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Update" class="btn btn-success pull-right"/>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                            </div>
                        </form>    
                    </div>
                </div>                 
            </div>                    
        </div>
    </div>
</div>
</div>
</div>

@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function(){
      $("#user_id").select2({
        placeholder: "Search User Type",
        allowClear: true,
        minimumInputLength: 2,
        width: null
    });

      $("#search-frm").submit(function(){
        oTableCustom.draw();
        return false;
    });


      $.fn.dataTableExt.sErrMode = 'throw';

      var oTableCustom = $('#server-side-datatables').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        pageLength: '{{ $length }}',
        displayStart: '{{ $start }}',
        ajax: {
            "url": "{!! route($moduleRouteText.'.data') !!}",
            "data": function ( data ) 
            {
                data.search_start_date = $("#search-frm input[name='search_start_date']").val();
                data.search_end_date = $("#search-frm input[name='search_end_date']").val();
                data.search_id = $("#search-frm input[name='search_id']").val();
                data.search_fnm = $("#search-frm input[name='search_fnm']").val();
                data.search_lnm = $("#search-frm input[name='search_lnm']").val();
                data.search_email = $("#search-frm input[name='search_email']").val();
                data.search_type = $("#search-frm select[name='search_type']").val();
                data.search_status = $("#search-frm select[name='search_status']").val();
            }
        },
        lengthMenu:
        [
        [25,50,100,150,200],
        [25,50,100,150,200]
        ],
        "order": [[ "{{ $orderClm }}", "{{ $orderDir }}" ]],
        columns: [
        { data: 'id', name: 'id' },
        { data: 'image', name:'image', orderable: false, searchable: false },
        { data: 'firstname', name: 'firstname' },
        { data: 'lastname', name: 'lastname' },
        { data: 'email', name: 'email' },
        { data: 'status', name: 'status' },
        { data: 'created_at', name: 'created_at' },
        { data: 'action', orderable: false, searchable: false}
        ]
    });
      $(document).on('click', '.change_user_password', function () 
      {
        var id = $(this).data('id');
        var href = $(this).data('content');
        $('#change_user_pass_frm').trigger('reset');
        $('#myModal').modal('show');
        $('#change_pass_id').val(id);
        $('#change_user_pass_frm').attr('action',href);

    });

      $('#change_user_pass_frm').submit(function () {
        
        if ($(this).parsley('isValid'))
        {
            $('#AjaxLoaderDiv').fadeIn('slow');
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: new FormData(this),
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                success: function (result)
                {
                    $('#AjaxLoaderDiv').fadeOut('slow');
                    if (result.status == 1)
                    {
                        $('#myModal').modal('hide');
                        $.bootstrapGrowl(result.msg, {type: 'success', delay: 4000});
                    }
                    else
                    {
                        $.bootstrapGrowl(result.msg, {type: 'danger', delay: 4000});
                    }
                },
                error: function (error) {
                    $('#AjaxLoaderDiv').fadeOut('slow');
                    $.bootstrapGrowl("Internal server error !", {type: 'danger', delay: 4000});
                }
            });
        }            
        return false;
    });
  });
</script>
@endsection