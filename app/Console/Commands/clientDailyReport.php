<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Models\ProjectClientUser;
use App\Models\ClientUser;  

class clientDailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:clientReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automation Process For sending daily work report to client.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get Tasks
        $sql = '
                SELECT  tasks.*,users.firstname,users.lastname,users.email as user_email,projects.title as project_name,projects.client_id,projects.send_email,projects.send_mail_type_all,clients.send_mail_type  
                    FROM tasks
                    JOIN users ON users.id = tasks.user_id
                    JOIN projects ON tasks.project_id = projects.id
                    JOIN clients ON clients.id = projects.client_id
                    WHERE tasks.report_sent = 0 AND clients.send_email = 1 AND projects.send_email = 1 
                    AND tasks.created_at >= ( CURDATE() - INTERVAL 2 DAY )
                    ORDER BY users.firstname, projects.client_id';          
          $rows = \DB::select($sql);

          // Get Admin Emails
          $admin_emails = User::getAdminEmails();
          $admin_names = User::getAdminName();

          $user_id = 0;
          $client_id = 0;
          $clients = array();
          $report_data=array();
          $report_all_data=array();
          $selectedUsers = array();
          $taskIdsArr = array();

        if(count($rows) > 0 ) 
        {
          foreach ($rows as $task_detail) 
          {
              $taskIdsArr[] = $task_detail->id;
              if($task_detail->send_mail_type == 0)
              {
                $selectedUsers = \DB::table(TBL_SEND_MAIL_USERS)
                        ->where("client_id",$task_detail->client_id)
                        ->pluck("user_id")
                        ->toArray();
              }

              if($task_detail->send_mail_type_all == 1)
              {
                if($task_detail->send_mail_type == 0 && !empty($selectedUsers) && is_array($selectedUsers))
                {
                  if(in_array($task_detail->user_id, $selectedUsers))
                  {
                    $report_all_data[$task_detail->user_id][$task_detail->client_id][]= $task_detail;
                  }
                }
                else
                {
                  $report_all_data[$task_detail->user_id][$task_detail->client_id][]= $task_detail;
                }
              }
              else 
              {
                if($task_detail->send_mail_type == 0 && !empty($selectedUsers) && is_array($selectedUsers))
                {
                  if(in_array($task_detail->user_id, $selectedUsers))
                  {
                    $report_data[$task_detail->user_id][$task_detail->client_id][$task_detail->project_id][]= $task_detail;
                  }
                }
                else
                {
                  $report_data[$task_detail->user_id][$task_detail->client_id][$task_detail->project_id][]= $task_detail;
                }
              }
          }

            \DB::table(TBL_TASK)
                ->whereIn("id",$taskIdsArr)
                ->update([
                  'report_sent' => 1,
                  'report_sent_date' => date('Y-m-d h:i:s'),
                ]);

            /*For all Client user Client wise Report Generate*/
            if(!empty($report_all_data))
            {
                foreach ($report_all_data as $user_id => $user_report) 
                {                   
                    foreach ($user_report as $client_id => $user_client_reportRow) 
                    {

                      $user_client_reportRow = json_decode(json_encode($user_client_reportRow),1);

                      $empName = "";

                      $table = "<p><b>Hi All,</b></p>";
                      $table .= "<p>Please find daily report below.</p>";

                      $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="font-size:13px; border-top:1px solid #666; border-left:1px solid #666; font-family:Arial, Helvetica, sans-serif;">';
                      $table .= "<tr>";
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Sr. No.</td>';
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Project</td>';
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Task/Feature</td>';
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Date</td>';
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Hour</td>';
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Status</td>';
                      $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Task Link</td>';
                      $table .= "</tr>";            $totalHours = 0;

                        $i = 1;

                        foreach($user_client_reportRow as $user_client_report )
                        {
                            $from_email = $user_client_report['user_email'];
                            $empName = ucfirst($user_client_report['firstname'])." ".ucfirst($user_client_report['lastname']);
                            $status = $user_client_report['status'] == 1 ? "DONE":"In Progress";

                            $table .= "<tr>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$i."</td>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$user_client_report['project_name']."</td>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$user_client_report['title']."</td>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.date("m/d/Y",strtotime($user_client_report['created_at']))."</td>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$user_client_report['total_time']."</td>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$status."</td>";
                            $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$user_client_report['ref_link']."</td>";
                            $table .= "</tr>";
                            $i++;

                            $totalHours += floatval($user_client_report['total_time']);
                        }  

                            $table .= "</table>";

                            $clientUsers_email = \DB::table("client_users")                          
                                    ->where("client_id",$client_id)
                                    ->where("send_email",1)
                                    ->pluck("email")
                                    ->toArray();
                            $clientUsers_name = \DB::table("client_users")                          
                                    ->where("client_id",$client_id)
                                    ->where("send_email",1)
                                    ->pluck("name","email")
                                    ->toArray();

                        $toEmails = array_merge($clientUsers_email,$admin_emails);
                        $params['to_email_names'] = array_merge($clientUsers_name,$admin_names);                
                            echo "Send Emails To:<br />";
                            echo "<pre>";
                            print_r( $toEmails);
                            echo "<pre>";
                            echo "HTML";


                            $table .= "<p>Thanks & Regards,<br />".$empName."</p>";
                            $subject = $empName.": Daily Report - (Hr-".$totalHours.") - ".date("j M, Y");
                            echo "<p>Subject: $subject</p>";
                            echo $table;
                        $params["to"]= $toEmails[0];
                        unset($toEmails[0]);
                        $params["ccEmails"]= $toEmails;
                        $params["subject"] = $subject;
                        $params["from"] = $from_email;
                        $params["from_name"] = $empName;  
                        $params["body"] = "<html><body>".$table."</body></html>";
                             
                        sendHtmlMail($params);                   
                    }
                }   
            }
            

            /*For selected Client User project wise Report Generate */
            if (!empty($report_data)) 
            {
                foreach ($report_data as $user_id => $user_report) 
                {                   
                    foreach ($user_report as $client_id => $client_report_data) 
                    { 
                        $client_report_data = json_decode(json_encode($client_report_data),1);

                        foreach ($client_report_data as $project_id => $project_report) 
                        {
                            $empName = "";

                            $table = "<p><b>Hi All,</b></p>";
                            $table .= "<p>Please find daily report below.</p>";

                            $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="font-size:13px; border-top:1px solid #666; border-left:1px solid #666; font-family:Arial, Helvetica, sans-serif;">';
                            $table .= "<tr>";
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Sr. No.</td>';
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Project</td>';
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Task/Feature</td>';
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Date</td>';
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Hour</td>';
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Status</td>';
                            $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Task Link</td>';
                            $table .= "</tr>";            $totalHours = 0;
                            $i = 1;
                            
                            foreach($project_report as $project_data )
                            {
                              $from_user_id = $project_data['user_id'];
                              $from_email = $project_data['user_email'];
                              $empName = ucfirst($project_data['firstname'])." ".ucfirst($project_data['lastname']);
                              $status = $project_data['status'] == 1 ? "DONE":"In Progress";

                              $table .= "<tr>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$i."</td>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$project_data['project_name']."</td>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$project_data['title']."</td>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.date("m/d/Y",strtotime($project_data['task_date']))."</td>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$project_data['total_time']."</td>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$status."</td>";
                              $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$project_data['ref_link']."</td>";
                              $table .= "</tr>";
                              $i++;

                              $totalHours += floatval($project_data['total_time']);
                            }

                            $project_client_users_email = ProjectClientUser::
                                            join(TBL_CLIENT_USER,TBL_PROJECT_CLIENT_USER.'.client_user_id',TBL_CLIENT_USER.'.id')
                                            ->join(TBL_CLIENT,TBL_CLIENT.'.id',TBL_CLIENT_USER.'.client_id')
                                            ->where(TBL_PROJECT_CLIENT_USER.".project_id",$project_id)
                                            ->pluck(TBL_CLIENT_USER.".email")
                                            ->toArray();
                            $project_client_users_name = ProjectClientUser::
                                            join(TBL_CLIENT_USER,TBL_PROJECT_CLIENT_USER.'.client_user_id',TBL_CLIENT_USER.'.id')
                                            ->join(TBL_CLIENT,TBL_CLIENT.'.id',TBL_CLIENT_USER.'.client_id')
                                            ->where(TBL_PROJECT_CLIENT_USER.".project_id",$project_id)
                                            ->pluck(TBL_CLIENT_USER.".name",TBL_CLIENT_USER.'.email')
                                            ->toArray();


                            $all_client_users_email =ClientUser::
                                           join(TBL_PROJECT,TBL_PROJECT.'.client_id',TBL_CLIENT_USER.'.client_id')
                                           ->join(TBL_CLIENT,TBL_CLIENT.'.id',TBL_CLIENT_USER.'.client_id')
                                           ->where(TBL_PROJECT.".id",$project_id)
                                           ->where(TBL_PROJECT.".send_email",1)
                                           ->where(TBL_PROJECT.".is_mail_all_user",1)
                                           ->pluck(TBL_CLIENT_USER.".email")
                                           ->toArray();
                            $all_client_users_name =ClientUser::
                                           join(TBL_PROJECT,TBL_PROJECT.'.client_id',TBL_CLIENT_USER.'.client_id')
                                           ->join(TBL_CLIENT,TBL_CLIENT.'.id',TBL_CLIENT_USER.'.client_id')
                                           ->where(TBL_PROJECT.".id",$project_id)
                                           ->where(TBL_PROJECT.".send_email",1)
                                           ->where(TBL_PROJECT.".is_mail_all_user",1)
                                           ->pluck(TBL_CLIENT_USER.".name",TBL_CLIENT_USER.'.email')
                                           ->toArray(); 
                  
                            $toEmails = array_merge($project_client_users_email,$admin_emails,$all_client_users_email);
                            $params['to_email_names'] = array_merge($project_client_users_name,$admin_names,$all_client_users_name);

                                echo "Send Emails To:<br />";
                                echo "<pre>";
                                print_r( $toEmails);
                                echo "<pre>";
                                echo "HTML";


                                 $table .= "</table>";
                                 $table .= "<p>Thanks & Regards,<br />".$empName."</p>";
                                 $subject = "Daily Report - (Hr-".$totalHours.") - ".date("j M, Y");
                                 echo "<p>Subject: $subject</p>";
                                 echo $table;
                             $params["to"]= $toEmails[0];
                             unset($toEmails[0]);
                             $params["ccEmails"]= $toEmails;
                             $params["subject"] = $subject;
                             $params["from"] = $from_email;
                             $params["from_name"] = $empName;  
                             $params["body"] = "<html><body>".$table."</body></html>";

                             sendHtmlMail($params);
                        }
                    }  
                }    
            }
        }
        exit();
    }
}
