<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminAction;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Models\Client; 
use DB;

class ProjectHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $checkrights = \App\Models\Admin::checkPermission(\App\Models\Admin::$PROJECT_WISE_HOURS);
        
        if($checkrights) 
        {
            return $checkrights;
        }
        
        $data['page_title'] = "Project wise monthly hours";
        $data['projects'] = \App\Models\Project::getList();
        $data['clients'] = Client::pluck("name","id")->all();

        $dates = \DB::table(TBL_TASK)->select(\DB::raw("MIN(task_date)as mindate,MAX(task_date) as maxdate"))->get();
        foreach ($dates as $date) 
        {
            $start_date = $date->mindate;
            $mindate = date_create($date->mindate);
            $maxdate = date_create($date->maxdate);
        } 
        
        //$maxdate->modify('+1 month');
        
        $data['task_data'] = [];
        $start_date = $start_date;
        $end_date = date('Y-m-d h:m:s');

        while (strtotime($start_date) <= strtotime($end_date))
        {
            $start_date = date('Y-M',strtotime($start_date));
            $data['task_data'][date('Y-m',strtotime($start_date))] = $start_date; 
            $start_date = date ("Y-M", strtotime("+1 month", strtotime($start_date)));
        }

        return view("admin.project_hours.index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data(Request $request)
    {
       $checkrights = \App\Models\Admin::checkPermission(\App\Models\Admin::$PROJECT_WISE_HOURS);
        
        if($checkrights) 
        {
            return $checkrights;
        }

       $model =Task::select(TBL_TASK.".*",TBL_CLIENT.".name as client_name",TBL_PROJECT.".title as project_name",\DB::raw('SUM(tasks.total_time) as total_hour, YEAR(tasks.task_date) as year'), \DB::raw("(DATE_FORMAT(".TBL_TASK.".task_date,'%Y, %M')) as month"))
            ->join(TBL_PROJECT,TBL_TASK.".project_id","=",TBL_PROJECT.".id")
            ->join(TBL_CLIENT,TBL_PROJECT.".client_id","=",TBL_CLIENT.".id")
            ->groupBy('tasks.project_id')
            ->groupBy(\DB::raw('YEAR(tasks.task_date) ASC, MONTH(tasks.task_date)'));

        $hours_query = Task::select(TBL_TASK.".*")
            ->join(TBL_PROJECT,TBL_TASK.".project_id","=",TBL_PROJECT.".id")
            ->join(TBL_CLIENT,TBL_PROJECT.".client_id","=",TBL_CLIENT.".id");
        
        $hours_query = Task::listFilter($hours_query);

        $totalHours = $hours_query->sum(TBL_TASK.".total_time");
        $totalHours = number_format($totalHours,2);

        $data =  \Datatables::eloquent($model)
            ->editColumn('task_date', function($row){
                if(!empty($row->task_date))          
                    return date("M, Y",strtotime($row->task_date));
                else
                    return '-';
            })->rawColumns(['task_date'])

        ->filter(function ($query) {
            $search_client = request()->get("search_client");
            $search_project = request()->get("search_project");
            $search_task_date = request()->get("search_task_date");
            $search_hour = request()->get("search_hour");
            $search_hour_op = request()->get("search_hour_op");
            $search_min = request()->get("search_min");

            $searchData = array();
            customDatatble(url('projects-hour'));

            if(!empty($search_client))
            {
                $query = $query->where(TBL_PROJECT.".client_id", $search_client);
                $searchData['search_client'] = $search_client;
            }
            if(!empty($search_project))
            {
                $query = $query->where(TBL_TASK.".project_id", $search_project);
                $searchData['search_project'] = $search_project;
            }
            if (!empty($search_task_date)) {
                $query = $query->where(TBL_TASK.".task_date", "LIKE", '%'.$search_task_date.'%');
                $searchData['search_task_date'] = $search_task_date;
            }
            $goto = \URL('projects-hour', $searchData);
            \session()->put(url('projects-hour'),$goto);
        });
        $data = $data->with('hours',$totalHours);
        $data = $data->make(true); 
        return $data;
    }
}
