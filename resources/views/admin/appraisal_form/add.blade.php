@extends('admin.layouts.app')
@section('styles')
 
@endsection

@section('content')
<div class="page-content">
    <div class="container">
        
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-file"></i>{{ $page_title }}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table">

                        {!! Form::model($formObj,['method' => $method,'files' => true, 'route' => [$action_url,$action_params],'class' => 'sky-form form form-group', 'id' => 'main-frm1']) !!} 
                        <table class="table table-bordered table-hover" id="invoice_table" width="100%">
                                <tr>
                                    <td colspan="2"><b style="font-size: 26px"><center>Appraisal Form</center></b></td>
                                </tr>
                                <tr>
                                    <td><b>1. Describe your duties and responsibilities. </b></td>
                                    <td> {!! Form::textarea('duty_responsibility',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>2. Total experience as on 31st December 2019. </b> (Ex. 5 years and 6 months)</td>
                                    <td> 
                                    <table>
                                        <tr>
                                            <td> {!! Form::text('years',null,['class' => 'form-control','data-required'=>true,'placeholder'=>'Year']) !!} </td>
                                            <td> Years </td>
                                            <td> {!! Form::text('months',null,['class' => 'form-control','data-required'=>true,'placeholder'=>'Month']) !!} </td>
                                            <td> Months </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>3. Rate your Past year </b>(0-3 Bad, 4-7 Average, 8-10 Good)</td>
                                    <td> {!! Form::select('past_year_rate',[''=>'Select Rate']+$past_year_rate,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>4. List your most significant achievement since last year. How does this achievement align with the goal outlined in your last review? </b></td>
                                    <td> {!! Form::textarea('achievements',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>5. Have you successfully performed any tasks or additional duties outside your regular responsibility, if so please specify. </b></td>
                                    <td> {!! Form::textarea('additional_duties',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>6. Rate yourself on all factor which describe below:</b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Technical Skill </td>
                                    <td> {!! Form::select('technical_skills',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> English communication with client </td>
                                    <td> {!! Form::select('english_communication',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Requirement understanding without help</td>
                                    <td> {!! Form::select('requirement_understanding',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Utilization of own productivity</td>
                                    <td> {!! Form::select('utilization_productivity',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Team work </td>
                                    <td> {!! Form::select('team_work',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Professionalism (Punctuality, attendance, responsiveness etc.) </td>
                                    <td> {!! Form::select('professionalism',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Can able to generate work from current client/ helping on client retention </td>
                                    <td> {!! Form::select('generate_work',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Work quality</td>
                                    <td> {!! Form::select('work_quality',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Git knowledge </td>
                                    <td> {!! Form::select('git_knowledge',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px;"> Attitude towards job/work </td>
                                    <td> {!! Form::select('attitude',[''=>'Select Rate']+$skills,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>7. How well do you prioritize and manage your workload? </b></td>
                                    <td> {!! Form::textarea('manage_workload',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>8. Do you feel that your achievements were recognised and rewarded? </b></td>
                                    <td> {!! Form::textarea('achievements_rewarded',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>9. Rate your job satisfaction </b>(0-3 Bad, 4-7 Average, 8-10 Good)</td>
                                    <td> {!! Form::select('job_satisfaction',[''=>'Select Rate']+$job_satisfaction,null,['class' => 'form-control', 'data-required' => true]) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>10. State your career goals for the coming year and describe how you plan to accomplish them.</b></td>
                                    <td> {!! Form::textarea('goal',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>11. What things can the company do to better your working environment? </b></td>
                                    <td> {!! Form::textarea('suggestion',null,['class' => 'form-control','data-required'=>true,'rows'=>3,'placeholder'=>'Type Here...']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right">
                                        <input type="hidden" id="is_submit" name="is_submit">
                                        <button type="button" name="save" data-id="0" class="btn btn-success save_submit">
                                        Save</button>
                                        <button type="button" name="save_submit" data-id="1" class="btn btn-primary save_submit">
                                        Save & Submit</button>
                                    </td>
                                </tr>
                        </table>

                    {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click','.save_submit',function(){
            var data_id = $(this).attr("data-id");
            $('#is_submit').val(data_id);
            $('#main-frm1').submit();
        });
         $('#main-frm1').submit(function () {
            
            if ($(this).parsley('isValid'))
            {
                $('#submit_btn').attr("disabled", true);
                $('#AjaxLoaderDiv').fadeIn('slow');
                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    enctype: 'multipart/form-data',
                    success: function (result)
                    {
                        $('#AjaxLoaderDiv').fadeOut('slow');
                        if (result.status == 1)
                        {
                            $.bootstrapGrowl(result.msg, {type: 'success', delay: 4000});
                            window.location.reload();    
                        }
                        else
                        {
                            $.bootstrapGrowl(result.msg, {type: 'danger', delay: 4000});
                            $('#submit_btn').attr("disabled", false);
                        }
                    },
                    error: function (error) {
                        $('#AjaxLoaderDiv').fadeOut('slow');
                        $.bootstrapGrowl("Internal server error !", {type: 'danger', delay: 4000});
                    }
                });
            }            
            return false;
        });
    });
</script>
 

@endsection