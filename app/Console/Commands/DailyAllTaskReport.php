<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class DailyAllTaskReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:taskReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily all tasks Report send to Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date("Y-m-d");

          // Get Tasks
         $sql = "
             SELECT  tasks.*,users.firstname,users.lastname,users.email as user_email,projects.title as project_name 
             FROM tasks
             JOIN users ON users.id = tasks.user_id
             JOIN projects ON tasks.project_id = projects.id              
             WHERE date_format(tasks.task_date, '%Y-%m-%d') = '".$date."'
             ORDER BY tasks.id
             ";
         $rows = \DB::select($sql);
         // Get Admin Emails
         $admin_emails = User::getAdminEmails();
         $admin_names = User::getAdminName();

         $user_id = 0;
         $client_id = 0;
         $totalHours = 0;
         $emp_time =0;
         $i = 1;        
         if(count($rows) > 0 ) 
         {
             $table = "<p><b>Hello Sir,</b></p>";
             $table .= "<p>Please find daily report below.</p>";
             $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="font-size:13px; border-top:1px solid #666; border-left:1px solid #666; font-family:Arial, Helvetica, sans-serif;">';
             $table .= "<tr>";
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Sr. No.</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Users Name</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Project</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Task/Feature</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Date</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Hour</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Status</td>';
             $table .= '<td width="9%" align="left" valign="middle" style="font-weight:600; background-color:#d9d9d9; border-right:1px solid #777; border-bottom:1px solid #777;">Task Link</td>';
             $table .= "</tr>";  

             foreach ($rows as $task_detail) 
             {   
                 $user_id = $task_detail->user_id;
                 $project_id=$task_detail->project_id;

                 $emp_data[$task_detail->user_id][]= $task_detail;
                 $emp_data[$task_detail->user_id]['emp_time'] = ($emp_data[$task_detail->user_id]['emp_time']??0)+$task_detail->total_time??0;
                 $emp_data[$task_detail->user_id]['emp_name'] =ucfirst($task_detail->firstname)." ".ucfirst($task_detail->lastname);

                 $report_data[$task_detail->user_id][$task_detail->project_id][]= $task_detail;
             }
              // dd($report_data);
              
             foreach ($report_data as $user_id => $user_report) 
             {   
                 $user_report=(array)$user_report;
                 $user_rowspan =array_sum(array_map("count",$user_report));
                                     
                 $empName = $emp_data[$user_id]['emp_name'];
                 $emp_time = $emp_data[$user_id]['emp_time'];

                 $table .= "<tr>";
                 $table .= '<td rowspan="'.$user_rowspan.'" align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$i."</td>";
                 $table .= '<td rowspan="'.$user_rowspan.'" align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$empName.' ['.$emp_time.']Hrs</td>';

                 $count=0;            
                 foreach ($user_report as $k=>$project_report) 
                 {
                     $project_report=(array)$project_report;
                     $project_rowspan=count($project_report);
                     $project_time=0;
                     $project_name=$project_report[0]->project_name;
                      
                     for ($s = 0; $s < $project_rowspan; $s++)
                     {
                         $project_time +=$project_report[$s]->total_time;
                         
                     } 
                     if($count!=0) 
                     {
                         $table.='</tr>';       
                         $table.='<tr>';       
                     }  


                     $table .= '<td align="left" rowspan="'.$project_rowspan.'" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$project_name." [".$project_time."]Hrs</td>";
                     
                     foreach ($project_report as $key =>$value) {
                         $value=(array)$value;
                         $emp_project_id =$value['project_id'];
                         $emp_project_task[$emp_project_id] = $value['project_name'];
                         $from_email = $value['user_email'];
                         $empName = ucfirst($value['firstname'])." ".ucfirst($value['lastname']);
                         $status = $value['status'] == 1 ? "DONE":"In Progress";
                         
                         if ($key!=0) 
                         {
                             $table.='</tr>';       
                             $table.='<tr>';       
                         }   
                         $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$value['title']."</td>";
                         $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.date("Y-m-d",strtotime($value['task_date']))."</td>";
                         $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$value['total_time']."</td>";
                         $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;">'.$status."</td>";
                         $table .= '<td align="left" valign="middle" style="border-right:1px solid #777; border-bottom:1px solid #777;"><a href="'.$value['ref_link'].'">'.$value['ref_link']."</a></td>";         

                         $totalHours += floatval($value['total_time']);                
                     }
                     ++$count;
                 }
                 $i++; 
                 $table .= "</tr>";
             }
              $table .= "</table>";
              $table .= "<p><b>Total Hours : ".$totalHours." </b> </p>";
              $table .= "<p>Thanks & Regards,<br />PHPDots Technologies.</p>";
              $toEmails = $admin_emails;               

             $subject = "Daily Report - (Hr-".$totalHours.") - ".date("j M, Y");

             $toEmail = [];
             $toEmails[0] = "jitendra.rathod@phpdots.com";
             $params["to"]= $toEmails[0];
             unset($toEmails[0]);
             // $params["ccEmails"]= $toEmails;
             $params["to_email_names"] = $admin_names;
             $params["subject"] = $subject;
             $params["from"] = $from_email;
             $params["from_name"] = 'PHPDots Technologies';  
             $params["body"] = "<html><body>".$table."</body></html>";  
             $data =array();
             $data['body']= $table;
             sendHtmlMail($params);
             $returnHTML = view('emails.index',$data)->render();
             //echo $returnHTML;
         }                            
     }                 
}
