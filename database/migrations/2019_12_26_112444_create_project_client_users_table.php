<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectClientUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_client_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('client_user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('client_user_id')
                  ->references('id')->on('client_users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('project_id')
                  ->references('id')->on('projects')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_client_users');
    }
}
