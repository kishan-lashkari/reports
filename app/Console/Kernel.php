<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\DailyAllTaskReport',
        'App\Console\Commands\SendDailyWorkReport',
		 'App\Console\Commands\dailyreport',
		'App\Console\Commands\DailyTaskNotAdded',
        'App\Console\Commands\MonthlyInstallment',
        'App\Console\Commands\MonthlyPenalty',
        'App\Console\Commands\MonthlyLoanPenalty',
        'App\Console\Commands\SoftwareExpireWeekly',
        'App\Console\Commands\clientDailyReport',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('software-licence:update')
                ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
