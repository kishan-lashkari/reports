<?php 
$rocords = \App\Models\InvoiceExpense::where('invoice_id',$row->id)->get();
?>
@if(count($rocords))

	<table class="auto-index" border="1">
		<tr>
			<th>Date</th>
			<th>Amount</th>
			<th>Status</th>
		</tr>
	@foreach ($rocords as $val)
		<tr>
			<td>{{$val->payment_date}}</td>
			<td>{{$val->amount}}</td>
			@if($val->payment_status==1)
				<td>Full</td>
			@else
				<td>Partials</td>
			@endif
		</tr>
	@endforeach
		</table>
@endif
