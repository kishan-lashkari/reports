<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToApprisalTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_forms', function (Blueprint $table) {
            $table->integer('technical_skills')->nullable();
            $table->integer('utilization_productivity')->nullable();
            $table->integer('team_work')->nullable();
            $table->integer('professionalism')->nullable();
            $table->text('manage_workload')->nullable();
            $table->text('achievements_rewarded')->nullable();
            $table->text('additional_duties')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_forms', function (Blueprint $table) {
            //
        });
    }
}
