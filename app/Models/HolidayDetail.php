<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HolidayDetail extends Model
{
	public $timestamps = true;
	protected $table = TBL_HOLIDAYS_DETAILS;
    /**
     * @var array
     */
    protected $fillable = ['holiday_id', 'date'];

    public static function dateDiffenece($date)
    {
    	$now = time();
    	$now_date = date('Y-m-d');
    	// $now_date = '2019-12-23';		
    	$your_date = strtotime($date);
    	$datediff = $now - $your_date;

		$is_sub = (date('D', strtotime('-2 days')) == 'Sun')?1:0;
		$is_sub += (date('D', strtotime('-1 days')) == 'Sun')?1:0;
		$is_sub += (date('D') == 'Sun')?1:0;

        $is_sat = (date('D', strtotime('-2 days')) == 'Sat')?1:0;
        $is_sat += (date('D', strtotime('-1 days')) == 'Sat')?1:0;
        $is_sat += (date('D') == 'Sat')?1:0;


    	$holiday_days = HolidayDetail::whereBetween('date',[$date,$now_date])->get();
    	$holidays = 0;
    	if($holiday_days){
    		foreach( $holiday_days as $holi) {
    			if (date('D',strtotime($holi->date)) == 'Sun' || date('D',strtotime($holi->date)) == 'Sat'){
    			}else{
    				$holidays += 1;
    			}
    		}
    	}
    	$days = round($datediff / (60 * 60 * 24));
    	$days = $days - $holidays - $is_sub - $is_sat;
    	if($days > 3){
    		return 0;
    	}else{
    		return 1;
    	}
    }
}
