<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectClientUser extends Model
{
   protected $table = TBL_PROJECT_CLIENT_USER;
   public $timestamps = true;

   protected $fillable = ['client_user_id','project_id'	];
}
